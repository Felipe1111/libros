/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.ArrayList;


/**
 *
 * @author LEIDY-MILENA
 */
public interface Crud {
    public abstract boolean agregar();
    public abstract boolean actualizar();
    public ArrayList consultar();
    public abstract boolean eliminar();
}
