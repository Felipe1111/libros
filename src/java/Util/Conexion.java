/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    public String driver, user, password, bd, urlB;
    public Connection conexion;
    
    public Conexion(){
        driver="com.mysql.jdbc.Driver";
        user="root";
        password="";
        bd="libreria";
        urlB="jdbc:mysql://localhost:3306/"+bd;
        
        try {
            Class.forName(driver).newInstance();
            conexion = DriverManager.getConnection(urlB, user, password);
            System.out.println("!Conexion OK¡");
        } catch (Exception e) {
            System.out.println("Error al conecctarse"+e.toString());
        }
    }
    
    public Connection obtenerConexion(){
       return conexion; 
    }
    
    public Connection cerrarConexion() throws SQLException{
        conexion.close();
        conexion = null;
        return conexion;
    }
    
    public static void main(String[] args) {
        new Conexion();
    }
}
